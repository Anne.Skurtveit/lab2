package INF101.lab2.pokemon;

public class Main {

    public static Pokemon pokemon1 = new Pokemon("Squirtle", 104, 10);
    public static Pokemon pokemon2 = new Pokemon("Oddish", 100, 3);
    public static void main(String[] args) {
           // Print initial states
        System.out.println(pokemon1);
        System.out.println(pokemon2);
   
           // Battle loop
        while (pokemon1.isAlive() && pokemon2.isAlive()) {
            pokemon1.attack(pokemon2);
            if (pokemon2.isAlive()) {
               pokemon2.attack(pokemon1);
            }
        }
   
        if (!pokemon1.isAlive()) {
            System.out.println(pokemon1.getName() + " is defeated by " + pokemon2.getName());
        } else {
            System.out.println(pokemon2.getName() + " is defeated by " + pokemon1.getName());
        }
    }
}
